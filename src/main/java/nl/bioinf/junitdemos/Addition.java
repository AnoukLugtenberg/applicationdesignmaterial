package nl.bioinf.junitdemos;

public class Addition {
    public int addNumbers(int a, int b) {
        int sum = a + b;
        return sum;
    }

    /* private --> jUnit test visible */
    int subtract(int a, int b) {
        return a - b;
    }

//    Not such a good idea after all because this places test code within production code
//    class SubtractTest {
//        @Test
//        public void testSubtract() {
//            System.out.println("testing subtract");
//            subtract(1, 2);
//        }
//    }
}