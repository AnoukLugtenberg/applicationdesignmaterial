package nl.bioinf.lambdas_streams_exercise;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Creation date: 7-2-2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
public class LambdasStreamExercise {
    public static final String DNA_ONE = "GTATTCATAC";
    public static final String DNA_TWO = "CATCATGAAATCGCTTGTCGCACTACTGCTGCTTTTAGTCGCTACTTCTGCCTTTGCTGACCAGTATGTAAATGGCTCTGGAGTCCTCTCCTTGCCAAA";
    public static final String DNA_THREE = "CATCATGAAATCGCTTGTCGCAYTACTGCTGCTTTTANTCGCTACTTCTGCCTTTGCTGACCKAGTATGTAAATGGCTCTGGAGTCCTCTCCTTGWCCAAA";

    public static String dnaToAbbreviatedNames(String DNA) {
        return DNA.chars()
                .mapToObj(nuc -> new Nucleotide((char) nuc))
                .map(nuc -> nuc.getName().substring(0, 3))
                .collect(Collectors.joining("."));
    }

    public static double dnaToWeight(String DNA) {
        return DNA.chars()
                .filter(nuc -> Nucleotide.isLegalNucleotide((char) nuc))
                .mapToObj(nuc -> new Nucleotide((char) nuc))
                .mapToDouble(Nucleotide::getWeight)
                .sum();
    }

    public static int countNucleotide(String DNA, char nucleotide) {
        return (int) DNA.chars()
                .mapToObj(nuc -> new Nucleotide((char) nuc))
                .filter(nuc -> nuc.getLetter() == nucleotide)
                .count();
    }

    public static void main(String[] args) {
        System.out.println("LambdasStreamExercise.dnaToAbbreviatedNames(\"AAACCCGGGTTT\") = " + LambdasStreamExercise.dnaToAbbreviatedNames(DNA_ONE));
        System.out.println("LambdasStreamExercise.dnaToWeight(DNA_ONE) = " + LambdasStreamExercise.dnaToWeight(DNA_ONE));
        System.out.println("LambdasStreamExercise.countNucleotide(DNA_ONE, 'A') = " + LambdasStreamExercise.countNucleotide(DNA_ONE, 'A'));
    }
}


