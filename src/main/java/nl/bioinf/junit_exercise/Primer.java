/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.junit_exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author michiel
 */
public class Primer {
    static final String CONSTRUCTOR_ARGUMENT_NULL = "cannot create primer instance with null as sequence";
    static final String CONSTRUCTOR_ARGUMENT_EMPTY = "cannot create primer instance with empty sequence";
    static final String CONSTRUCTOR_ARGUMENT_CONTAINS_INVALID_CHARS = "cannot create primer instance with invalid characters";
    private static final int DEFAULT_PRIMER_LENGTH = 20;
    private String sequence;
    public static final char[] NUCLEOTIDES = new char[]{'A', 'C', 'G', 'T'};
    public static final Random RANDOM = new Random();

    public Primer(String sequence) {
        checkValidInput(sequence);
        this.sequence = sequence.toUpperCase();
        checkValidNucleotidesOfSequence(sequence);

    }

    private void checkValidNucleotidesOfSequence(String sequence) {
        Pattern p = Pattern.compile("[^ACGT]");
        Matcher m = p.matcher(sequence);
        if (m.find()) {
            throw new IllegalArgumentException(CONSTRUCTOR_ARGUMENT_CONTAINS_INVALID_CHARS);
        }
    }

    private void checkValidInput(String sequence) {
        if (null == sequence) {
            throw new IllegalArgumentException(CONSTRUCTOR_ARGUMENT_NULL);
        } else if (sequence.isEmpty()) {
            throw new IllegalArgumentException(CONSTRUCTOR_ARGUMENT_EMPTY);
        }
    }

    public Primer() {
    }

    public double getGcPercentage() {
        //solve this the Java 8 way
        final int[] gcCount = new int[]{0};
        this.sequence.chars().forEach(
                (n) -> {
                    if (n == 67 || n == 71) {
                        gcCount[0]++;
                    }
                }
        );
        return ((double) gcCount[0] / this.sequence.length()) * 100;
    }

    /**
     * This very simple method assigns 2°C to each A-T pair and 4°C to each G-C pair. The Tm then is the sum of these
     * values for all individual pairs in a DNA double strand. This takes into account that the G-C bond is stronger
     * than the A-T bond. Note that the 2+4 rule is valid for a small length range only, about 20-40 nt.
     *
     * @return meltTemp the melting temperature
     */
    public double getMeltingTemperature() {
        //solve this the Java 8 way
        final int[] gcCount = new int[]{0};
        this.sequence.chars().forEach(
                (n) -> {
                    if (n == 67 || n == 71) {
                        gcCount[0]++;
                    }
                }
        );
        double mt = (gcCount[0] * 4) + ((this.sequence.length() - gcCount[0]) * 2);
        return mt;
    }

    public int getLength() {
        return sequence.length();
    }

    public String getSequence() {
        return sequence;
    }

    @Override
    public String toString() {
        return "Primer{GC%=" + getGcPercentage()
                + ", Tm=" + getMeltingTemperature()
                + ", length=" + sequence.length() + '}'
                + ", sequence=" + sequence.toString() + '}';
    }

    static List<Primer> createPrimerCollection(int listLength) {
        List<Primer> primers = new ArrayList<>();
        for (int i = 0; i < listLength; i++) {
            Primer primer = createRandomPrimer(DEFAULT_PRIMER_LENGTH);
            primers.add(primer);
        }
        return primers;
    }

    public static Primer createRandomPrimer(int primerLength) {
        if (primerLength < 0) {
            throw new IllegalArgumentException("Cannot create sequence of negative length (given: " + primerLength + ")");
        }
        StringBuilder seq = new StringBuilder();
        for (int j = 0; j < primerLength; j++) {
            int selected = RANDOM.nextInt(NUCLEOTIDES.length);
            char nuc = NUCLEOTIDES[selected];
            seq.append(nuc);
        }
        return new Primer(seq.toString());
    }


    public static void main(String[] args) {

        Primer p = new Primer("AAAGGGCCCTTTT");
        double gc = p.getGcPercentage();
        System.out.println("gc = " + gc);
    }
}
