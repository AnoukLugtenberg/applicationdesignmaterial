package nl.bioinf.designpatterns.observer_filter;

/**
 * Models a sequence object.
 * Is implemented as immutable object.
 * Created by michiel on 07/03/2017.
 */
public class Sequence {
    private String sequence;
    private String quality;
    private long id;


    /**
     * construct with all relevant fields.
     *
     * @param sequence
     * @param quality
     * @param id
     */
    public Sequence(String sequence, String quality, long id) {
        this.sequence = sequence;
        this.quality = quality;
        this.id = id;
    }

    public String getSequence() {
        return sequence;
    }

    public String getQuality() {
        return quality;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "sequence='" + sequence + '\'' +
                ", quality='" + quality + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sequence sequence = (Sequence) o;

        return id == sequence.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
