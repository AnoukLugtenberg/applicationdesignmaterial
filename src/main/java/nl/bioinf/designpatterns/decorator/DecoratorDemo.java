package nl.bioinf.designpatterns.decorator;

/**
 * Creation date: Jul 08, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class DecoratorDemo {
    public static void main(String[] args) {
//create coffee with milk and sugar
        Beverage coffee = new MilkDecorator(new SugarDecorator(new Coffee()));
        Beverage milkCoffee = new MilkDecorator(new Coffee());
        Beverage regularCoffee = new Coffee();
        Beverage sugarDecorator = new SugarDecorator(new Tea());
        Beverage tea = new Tea();
        tea.prepare();
        sugarDecorator.prepare();
        milkCoffee.prepare();
        coffee.prepare();

//create tea with sugar only
        Beverage teaWithSugar = new SugarDecorator(new Tea());
        teaWithSugar.prepare();
    }
}
