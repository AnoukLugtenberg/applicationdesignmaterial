package nl.bioinf.designpatterns.strategy;

/**
 * Created by michiel on 02/03/2017.
 */
public class Kiwi extends AbstractBird {
    public Kiwi() {
        super.setFlyBehaviour(new NoFlyBehaviour());
        super.setName("Kiwi");
    }
}
