package nl.bioinf.fp_demos;

import nl.bioinf.fp_exercise.Address;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


public class StreamsDemo {
    public static final String[] words = {"arg", "bah", "yeah", "howwie", "aw", "whoa", "yuck"};

    public static void main(String[] args) {
        StreamsDemo demo = new StreamsDemo();
//        demo.simpleStream();
//        demo.streamToStream();
//        demo.streamToOptional();
//        demo.streamToCollector();
//        demo.toSumReduction();
//        demo.frequencyReduction();
        demo.customObjectsStream();
    }


    public void simpleStream() {

        Arrays.stream(words).forEach(w -> System.out.println(w));

        //List<String> list = Arrays.asList(words);
        //list.stream().forEach(w -> System.out.println(w));
    }

    public void streamToStream() {
        Arrays.stream(words).map(w -> w.length()).forEach(System.out::println);
    }

    public void streamToOptional() {
        System.out.println(Arrays.stream(words)
                .map(String::length)
                .max((one, two) -> Integer.compare(one, two)).get());
        //same as (Integer::compare).get());
    }

    public void streamToCollector() {
        List<Integer> collected = Arrays.stream(words)
                .map(String::length)
                .distinct()
                .collect(Collectors.toList());
        System.out.println("collected = " + collected);
    }

    public void toSumReduction() {
        Optional<Integer> sum = Arrays.stream(words)
                .map(String::length)
                .reduce((x, y) -> x + y);
        System.out.println("sum.get() = " + sum.get());
    }

    public void frequencyReduction() {
        Map<Integer, Long> collect = Arrays.stream(words)
                .map(String::length)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        //similar to (only Integers)
        Map<Integer, Integer> collectAlt = Arrays.stream(words)
                .map(String::length)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.summingInt(e -> 1)));

        System.out.println("collect = " + collect);
        System.out.println("collect = " + collectAlt);
    }

    public void customObjectsStream() {
        FIdemo.USERS.stream()
                .map((u) -> {
                    if (u.getAddress() == null) {
                        Address a = new Address();
                        a.setStreet("Homeless");
                        return a;
                    } else {
                        return u.getAddress();
                    }
                })
                .distinct()
                .forEach(a -> System.out.println(a.getStreet()));
    }
}
