package nl.bioinf.gerommel;

import nl.bioinf.fp_demos.FIdemo;
import nl.bioinf.fp_exercise.User;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Gerommel {

    public static void main(String[] args) {
        String input = "de kat krabt de krullen van de trap";

        String singleInput = "krullen";

        Function<String, Word> wordToWord = (i) -> new Word(i);

        Supplier<Integer> randomNumberSupplier = () -> (int) (Math.random() * 5);

        BiFunction<Integer, String, Word> myStringCombiner = (i, s) -> new Word(i + " " + s);

        Word w2 = myStringCombiner.apply(3, "the");

        for (int i = 0; i < 5; i++) {
            System.out.println("Number generated: " + randomNumberSupplier.get());
        }

        Word w = wordToWord.apply(singleInput);
        System.out.println("w = " + w);
        System.out.println("w2 = " + w2);

        Arrays.stream(input.split(" "))
                .filter(word -> word.length() > 2)
                .map(word -> randomNumberSupplier.get() + " " + word)
                .map(word -> new Word(word))
                .map(word -> word.reverse())
                .forEach(word -> System.out.println(word));

        //Grouping by example
        Map<Integer, List<String>> wordLengthMap = Arrays.stream(input.split(" "))
                .collect(Collectors.groupingBy(x -> x.length()));

        wordLengthMap.forEach((x, y) -> System.out.println(x + ": " + y.stream().collect(Collectors.joining(" "))));

        //Optional example

        Stream.of(1, 2, 3, 42).map(x -> getUser(x))
                .map(o -> o.orElse(User.DUMMY))
                .forEach(x -> System.out.println(x.getName()));
    }


    //Optional example
    private static Optional<User> getUser(int id) {
        if (id == 42) {
            return Optional.of(FIdemo.USERS.get(0));
        }
        else return Optional.empty();
    }


    private static class Word {
        private final String word;

        public Word(String theWord) {
            this.word = theWord;
        }

        public Word reverse() {
            return new Word(new StringBuilder(word).reverse().toString());
        }

        @Override
        public String toString() {
            return "The word is: " + this.word;
        }
    }
}

