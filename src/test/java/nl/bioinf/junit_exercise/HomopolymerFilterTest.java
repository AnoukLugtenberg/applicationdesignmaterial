package nl.bioinf.junit_exercise;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HomopolymerFilterTest {

    @Test
    void isOKWithNonHomopolymerSunny() {
        Primer testPrimer = new Primer("AAACCCGGGTTT");
        HomopolymerFilter hf = new HomopolymerFilter();
        boolean expected = true;
        boolean given = hf.isOK(testPrimer);
        assertEquals(expected, given);
    }

    @Test
    void isOKWithHomopolymerSunny() {
        Primer testPrimer = new Primer("ATATAT");
        HomopolymerFilter hf = new HomopolymerFilter();
        boolean expected = false;
        boolean given = hf.isOK(testPrimer);
        assertEquals(expected, given);
    }



}
