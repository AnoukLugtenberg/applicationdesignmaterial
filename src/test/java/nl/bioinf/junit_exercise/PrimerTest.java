package nl.bioinf.junit_exercise;

import org.junit.jupiter.api.Test;

import java.util.List;

import static nl.bioinf.junit_exercise.Primer.CONSTRUCTOR_ARGUMENT_CONTAINS_INVALID_CHARS;
import static nl.bioinf.junit_exercise.Primer.CONSTRUCTOR_ARGUMENT_EMPTY;
import static org.junit.jupiter.api.Assertions.*;

public class PrimerTest {

    private static final double DELTA = 1e-4;
    @Test
    public void getGcPercentageSunny() {
        final String input = "ggggccccaaaatttt";
        final double expectedPercentage = 50.0;
        Primer testPrimer = new Primer(input);
        double observedPercentage = testPrimer.getGcPercentage();
        assertEquals(expectedPercentage, observedPercentage, DELTA);
    }

    @Test
    public void getGcPercentageSequenceIsNull() {
        final String input = null;
        Throwable exception = assertThrows(
                IllegalArgumentException.class,
                () -> new Primer(input).getGcPercentage());
                assertEquals(exception.getMessage(), "cannot create primer instance with null as sequence" );
    }

    @Test
    public void getGcPercentageSequenceIsEmpty() {
        final String input = "";
        Throwable exception = assertThrows(
                IllegalArgumentException.class,
                () -> new Primer(input).getGcPercentage());
        assertEquals(exception.getMessage(), CONSTRUCTOR_ARGUMENT_EMPTY);
    }

    @Test
    void createPrimerSunny() {
        final String input = "AAACCCCGGGGAAA";
        Primer testPrimer = new Primer(input);
        String observedPrimer = "AAACCCCGGGGAAA";
        assertEquals(testPrimer.getSequence(), observedPrimer);
    }

    @Test
    void createPrimerWithWrongNucleotides() {
        final String input = "EEEWWW";
        Throwable exception = assertThrows(
                IllegalArgumentException.class,
                () -> new Primer(input)
        );
        assertEquals(exception.getMessage(), CONSTRUCTOR_ARGUMENT_CONTAINS_INVALID_CHARS);
    }

    @Test
    void createRandomPrimerSunny() {
        int expectedPrimerLength = 20;
        Primer testPrimer = Primer.createRandomPrimer(expectedPrimerLength);
        int observedPrimerLength = testPrimer.getLength();
        assertEquals(expectedPrimerLength, observedPrimerLength);
    }

    @Test
    void createRandomPrimerNegativeInt() {
        int requestedLength = -1;

        //Other way to do it (can also be used in test)
//        try {
//            Primer testPrimer = Primer.createRandomPrimer(requestedLength);
//        } catch (IllegalArgumentException ex) {
//            assertTrue
//                    (true);
//            return;
//        }
//        fail("No exception generated");

        //JUnit 5
        Throwable exception = assertThrows(
                IllegalArgumentException.class,
                () -> Primer.createRandomPrimer(requestedLength));
        assertEquals(exception.getMessage(), "Cannot create sequence of negative length (given: " + requestedLength + ")");
    }

    @Test
    void testFilterRandomPrimersJava8() {
        List<Primer> testCollection = Primer.createPrimerCollection(20);
        testCollection.stream().peek(System.out::println)
                .filter(p -> p.getMeltingTemperature() > 50 && p.getMeltingTemperature() < 60)
                .filter(this::containsAllFourNucleotides)
                .forEach(System.out::println);
    }

    private boolean containsAllFourNucleotides(Primer p) {
        String seq = p.getSequence();
        System.out.println(seq.contains("Q"));
        return seq.contains("Q");
    }


    @Test
    void createPrimerCollectionSunny() {
        List<Primer> primers = Primer.createPrimerCollection(3);
        int expectedListLength = 3;
        int observedListLength = primers.size();
        assertEquals(expectedListLength, observedListLength);
    }

    @Test
    void getMeltingTemperatureSunny() {
        Primer testPrimer = new Primer("AAAGGGCCCTTTT");
        double expectedMeltingTemperature = 46.1538;
        double observedMeltingTemperature = testPrimer.getGcPercentage();
        assertEquals(expectedMeltingTemperature, observedMeltingTemperature, DELTA);
    }
}