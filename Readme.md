# Application Design Material

## Purpose
This repo holds all the code used to demonstrate and train the principles, patterns and
 practices in the course "Application Design" which is organized in the Application 
 Design minor of bio-informatics at the Hanze University
 of Applied Science Groningen.  
 
 *Copyright 2017 Michiel Noback, Hanze University*

## Instructions

Clone it and open the build.gradle as new project in IntelliJ Idea
 
## Contents

There are several packages, some of which serve as demo material only,
 but there are also packages containing exercise, such as `nl.bioinf.junit_exercise` and 
 `nl.bioinf.functional_programming`, with their own readme with instructions on the exercises.
 
## Exercises of the course Application Design

1. **Code analysis exercise**  
  Study one of your previous (Java) projects and describe where you think your code is 
  faulty or inflexible or poorly designed.

2. **JUnit Exercises (5 exercises)**  
Go to package nl.bioinf.junit_exercise and follow the instructions given therein.

3. **Datamodel design**    
  Design the datamodel for a bird monitoring application. You do not need to be extremely detailed 
    about the methods of classes; only provide the key methods.
    In the bird monitoring app, users should be able to enter bird sightings, and later be able 
    to search/browse the sightings database on these aspects:  
    - Area
    - Species group
    - Rarity of species
    - Whether there are photos uploaded with the sighting
    - Maybe you can think of some other features as well?

4. **Lambdas and Streams (3 exercises)**  
  Go to package nl.bioinf.lambdas_streams_exercise and follow the instructions given therein.

5. **Immutable Class Design**  
  Go to package nl.bioinf.fp_exercise and follow the instructions given therein.

6. **Refactoring**  
  Revisit exercise 1, where you studied one of your previous (Java) projects
  and described where you think your code was faulty or inflexible or poorly designed.
  Perform three refactoring operations on this code, and apply at least one Design Pattern.
  Report these refactorings; show code before and after and describe the refactorings you
  applied with their rationale.




 